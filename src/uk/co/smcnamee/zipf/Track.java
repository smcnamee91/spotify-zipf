package uk.co.smcnamee.zipf;

/**
 * Created by a543097 on 04/12/2014.
 */
public class Track {

    int plays;
    String name;

    public Track(int plays, String name){
        this.name = name;
        this.plays = plays;
    }

}
